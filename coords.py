class Coords:
    
    def __init__(self, x, y, terrain, occupied):
        self.x = x
        self.y = y
        self.terrain = terrain
        self.occupied = occupied

    # Return properties
    def getX():
        return x

    def getY():
        return y

    def getTerrain():
        return terrain

    def getOccupied():
        return occupied
