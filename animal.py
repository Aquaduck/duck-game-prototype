#!/usr/bin/env python3

class Animal:

    def __init__(self, coords, energy, terrain, hunger, thirst, repro):
       self.coords = coords
       self.energy = energy
       self.terrain = terrain   # Terrain able to be traveled on
       self.hunger = hunger
       self.thirst = thirst
       self.repro = repro       # Reproductive urge

    def getCoords():
        return coords

    def getEnergy():
        return energy

    def getTerrain():
        return terrain

    def getHunger():
        return hunger

    def getThirst():
        return thirst

    def getRepro():
        return repro
